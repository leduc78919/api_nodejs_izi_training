const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { baiTapService } = require('../services/index');
const { http } = require('../config/logger');


const cau1 = (async (req, res) => {
  const lops = await baiTapService.cau1();
  res.status(httpStatus.OK).send(lops);
});

const cau2 = (async (req, res) => {
  const result = await baiTapService.cau2();
  res.status(httpStatus.OK).send(result);
});

const cau3 = (async (req, res) => {
  const result = await baiTapService.cau3();
  res.status(httpStatus.OK).send(result);
});

const cau4 = (async (req, res) => {
  const result = await baiTapService.cau4();
  res.status(httpStatus.OK).send(result);
});

const cau5 = (async (req, res) => {
  const result = await baiTapService.cau5();
  res.status(httpStatus.OK).send(result);
});

const cau6 = (async (req, res) => {
  const result = await baiTapService.cau6();
  res.status(httpStatus.OK).send(result);
});

const cau7 = (async (req, res) => {
  const result = await baiTapService.cau7();
  res.status(httpStatus.OK).send(result);
});

const cau8 = (async (req, res) => {
  const result = await baiTapService.cau8();
  res.status(httpStatus.OK).send(result);
});

const cau9 = (async (req,res) => {
  const result = await baiTapService.cau9();
  res.status(httpStatus.OK).send(result);
})

const cau10 = (async(req,res) => {
  const result = await baiTapService.cau10();
  res.status(httpStatus.OK).send(result);
})

const cau11 = async(req,res) => {
  const result = await baiTapService.cau11();
  res.status(httpStatus.OK).send(result);
}

const cau12 = async(req,res) => {
  const result = await baiTapService.cau12();
  res.status(httpStatus.OK).send(result);
}

const cau13 = async(req,res) => {
    const result = await baiTapService.cau13();
    res.status(httpStatus.OK).send(result);
}

const cau14 = async(req,res) => {
  const result = await baiTapService.cau14();
  res.status(httpStatus.OK).send(result);
}

const cau15 = async(req,res) => {
  const result = await baiTapService.cau15();
  res.status(httpStatus.OK).send(result);
}

const cau16 = async(req,res) => {
  const result = await baiTapService.cau16();
  res.status(httpStatus.OK).send(result);
}

const cau17 = async(req,res) => {
  const result = await baiTapService.cau17();
  res.status(httpStatus.OK).send(result);
}

const cau18 = async(req,res) => {
  const result = await baiTapService.cau18();
  res.status(httpStatus.OK).send(result);
}

const cau19 = async(req,res) => {
  const result = await baiTapService.cau19();
  res.status(httpStatus.OK).send(result);
}

const cau20 = async(req,res) => {
  const result = await baiTapService.cau20();
  res.status(httpStatus.OK).send(result);
}

const cau21 = async(req,res) => {
  const result = await baiTapService.cau21();
  res.status(httpStatus.OK).send(result);
}

const cau22 = async(req,res) => {
  const result = await baiTapService.cau22();
  res.status(httpStatus.OK).send(result);
}

const cau23 = async(req,res) => {
  const result = await baiTapService.cau23();
  res.status(httpStatus.OK).send(result);
}

const cau24 = async(req,res) => {
  const result = await baiTapService.cau24();
  res.status(httpStatus.OK).send(result);
}

const cau25 = async(req,res) => {
  const result = await baiTapService.cau25();
  res.status(httpStatus.OK).send(result);
}


module.exports = {
  cau1,
  cau2,
  cau3,
  cau4,
  cau5,
  cau6,
  cau7,
  cau8,
  cau9,
  cau10,
  cau11,
  cau12,
  cau13,
  cau14,
  cau15,
  cau16,
  cau17,
  cau18,
  cau19,
  cau20,
  cau21,
  cau22,
  cau23,
  cau24,
  cau25,
};
