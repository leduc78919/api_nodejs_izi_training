module.exports.Token = require('./token.model');
module.exports.User = require('./user.model');

module.exports.khoa = require('./khoa.model');
module.exports.lop = require('./lop.model');
module.exports.monHoc = require('./monHoc.model');
module.exports.sinhVien = require('./sinhVien.model');
module.exports.ketQua = require('./ketQua.model');
