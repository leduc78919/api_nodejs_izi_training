const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const ketQuaSchema = new mongoose.Schema(
  {
    MaSV: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "sinhVien",
    },
    MaMH: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "monHoc",
    },
    DiemThi: {
      type: Number,
      require: true,
      min: 0,
    }
  }
);

// add plugin that converts mongoose to json
ketQuaSchema.plugin(toJSON);


const ketQua = mongoose.model('ketQua', ketQuaSchema);

module.exports = ketQua;
