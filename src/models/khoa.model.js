const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const khoaSchema = new mongoose.Schema(
  {
    TenKhoa: {
      type: String,
      trim: true,
      required: true,
    },
    SoCBGD: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
khoaSchema.plugin(toJSON);

const khoa = mongoose.model('khoa', khoaSchema);

module.exports = khoa;
