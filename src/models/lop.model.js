const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { toJSON } = require('./plugins');

const lopSchema = new Schema(
  {
    TenLop: {
      type: String,
      trim: true,
      required: true,
    },
    MaKhoa: {
      type: Schema.Types.ObjectId,ref: "khoa"
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
lopSchema.plugin(toJSON);

const lop = mongoose.model('lop', lopSchema);

module.exports = lop;
