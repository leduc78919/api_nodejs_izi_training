const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const monHocSchema = new mongoose.Schema(
  {
    TenMH: {
      type: String,
      trim: true,
      require: true,
    },
    SoTiet: {
      type: Number,
      min : 1,
      require: true,

    }
  }
);

// add plugin that converts mongoose to json
monHocSchema.plugin(toJSON);


const monHoc = mongoose.model('monHoc', monHocSchema);

module.exports = monHoc;
