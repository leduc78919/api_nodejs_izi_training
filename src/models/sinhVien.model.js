const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const sinhVienSchema = new mongoose.Schema(
  {
    HoTen: {
      type: String,
      trim: true,
      required: true,
    },
    Nu: {
      type: String,
      trim: true,
      required: true,
    },
    NgaySinh: {
      type: Date,
      max: new Date(),
      required: true,
    },
    MaLop: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'lop'
    },
    HocBong: {
      type: Number,
      min : 0,
    },
    Tinh: {
      type: String,
      trim: true,
    }
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
sinhVienSchema.plugin(toJSON);


const sinhVien = mongoose.model('sinhVien', sinhVienSchema);

module.exports = sinhVien;
