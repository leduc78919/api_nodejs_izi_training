const mongoose = require('mongoose');
const { ketQua, lop, khoa, monHoc,sinhVien} = require('../models/index');



  const cau1 = async () => {
    return lop.find();
  };

  const cau2 = async () => {
    return sinhVien.find({}).select('HoTen HocBong  x');
  }

  const cau3 = async () => {
    return sinhVien.find({HocBong : {$gt: '0'}}).select('Nu HocBong');
  }

  const cau4 = async () => {
    return sinhVien.find({Nu : "Yes"});
  }

  const cau5 = async () => {
    return sinhVien.find({HoTen : { $regex: '^Trần'}});
  }

  const cau6 = async () => {
    return sinhVien.find({'Nu': "Yes", 'HocBong': { $gt: 0}});
  }

  const cau7 = async () => {
    return sinhVien.find({$or:[{'Nu' : "Yes"}, {'HocBong' : {$gt : 0}}]});
  }

  const cau8 = async() => {
    return sinhVien.find({'NgaySinh' : {$gte: "1978-01-01", $lte: "1985-01-01"}});
  }

  const cau9 = async() => {
    return sinhVien.find({}).sort({'_id' : 1});
  }

  const cau10 = async() => {
    return sinhVien.find({}).sort({'HocBong' : -1});
  }

  const cau11 = async () => {
    const result = sinhVien.aggregate([
      {
        $lookup: {
          from: ketQua.collection.name,
          localField: "_id",
          foreignField: "MaSV",
          as: "ketQua"
        },
      },
      {
        $replaceRoot: {newRoot: {$mergeObjects: [{$arrayElemAt: ["$ketQua",0]}, "$$ROOT"]}}
      },
      {
        $project: {ketQua: 0}
      },
      {
        $match: {
          $and: [{DiemThi: {$gte: 8}}, {MaMH: mongoose.Types.ObjectId('63be8612a0e8ec4a4d3b8470')}]
        },
      },
      {
        $project: { _id: 1, HoTen: 1,Nu:1, NgaySinh:  1 , DiemThi:1},
      }
    ]);
    return result;
  }

  const cau12 = async() => {
    const result = sinhVien.aggregate([
      {
        $lookup: {
          from: lop.collection.name,
          localField: "MaLop",
          foreignField: "_id",
          as: "lop",
        },
      },
      {
        $replaceRoot: { newRoot: {$mergeObjects: [{$arrayElemAt: ["$lop",0]}, "$$ROOT"]}}
      },
      {
        $project: {lop : 0}
      },
      {
        $match: {
          $and: [{HocBong: {$gt : 0}}, {MaKhoa: mongoose.Types.ObjectId( '63be858a730f003625a66e07')}],
        }
      },
      {
        $project: {TenLop : 1,HoTen: 1, HocBong:1},
      },

    ])
    return result;
  }

  const cau13 = async() => {
    const result = sinhVien.aggregate([
      {
        $lookup: {
          from: lop.collection.name,
          localField: "MaLop",
          foreignField: "_id",
          as: "lop",
        }
      },
      {
        $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ["$lop",0]}, "$$ROOT"]} }
      },
      {
        $project: {lop : 0},
      },
      {
        $lookup: {
          from: khoa.collection.name,
          localField: "MaKhoa",
          foreignField: "_id",
          as: "khoa",
        },
      },
      {
        $replaceRoot : { newRoot: { $mergeObjects: [{ $arrayElemAt: ["$khoa",0] }, "$$ROOT"]}},
      },
      {
        $project: {khoa : 0},
      },
      {
        $match: {
          $and: [{HocBong: {$gt: 0}}, {MaKhoa: mongoose.Types.ObjectId('63be858a730f003625a66e07')}],
        }
      },
      {
        $project: {HoTen:1, HocBong: 1,TenLop: 1, TenKhoa: 1},
      }
    ]);
    return result;
  }

  const cau14 = async() => {
    const result = sinhVien.aggregate([
      {
        $group: {
          _id: "$MaLop",
          count: {$sum : 1},
        }
      },
      {
        $lookup: {
          from: lop.collection.name,
          localField: "_id",
          foreignField: "_id",
          as: "lop",
        },
      },
      {
        $replaceRoot : { newRoot: { $mergeObjects: [{$arrayElemAt : ["$lop",0]}, "$$ROOT"] } },
      },
      {
        $project: {lop: 0},
      },
      {
        $project: {TenLop: 1, count: 1},
      }
    ])
    return result;
  }

  const cau15 = async() => {
    const result = sinhVien.aggregate([
      {
        $lookup: {
          from: lop.collection.name,
          localField: "MaLop",
          foreignField: "_id",
          as: "lop",
        }
      },
      {
        $replaceRoot: {newRoot: {$mergeObjects: [{$arrayElemAt: ["$lop",0]}, "$$ROOT"]} },
      },
      {
        $project: {lop: 0},
      },
      {
        $group: {
          _id: "$MaKhoa",
          count: {$sum: 1},
        }
      },
      {
        $lookup: {
          from: khoa.collection.name,
          localField: "_id",
          foreignField: "_id",
          as: "khoa",
        }
      },
      {
        $replaceRoot: {newRoot: {$mergeObjects: [{$arrayElemAt: ["$khoa",0]}, "$$ROOT"]} },
      },
      {
        $project: {khoa: 0},
      },
      {$project: {TenKhoa: 1, count: 1}}
    ]);
    return result;
  }

  const cau16 = async() => {
    const result = sinhVien.aggregate([
      {
        $lookup: {
          from: lop.collection.name,
          localField: "MaLop",
          foreignField: "_id",
          as: "lop",
        }
      },
      {
        $match: {
          Nu: "Yes",
        }
      },
      {
        $replaceRoot: {newRoot: {$mergeObjects: [{$arrayElemAt: ["$lop",0]}, "$$ROOT"]} },
      },
      {
        $project: {lop: 0},
      },
      {
        $group: {
          _id: "$MaKhoa",
          SLsinhvienNu: {$sum: 1},
        }
      },
      {
        $lookup: {
          from: khoa.collection.name,
          localField: "_id",
          foreignField: "_id",
          as: "khoa",
        }
      },
      {
        $replaceRoot: {newRoot: {$mergeObjects: [{$arrayElemAt: ["$khoa",0]}, "$$ROOT"]} },
      },
      {
        $project: {khoa: 0},
      },
      {$project: {TenKhoa: 1, SLsinhvienNu: 1}}
    ]);
    return result;
  }

  const cau17 = async() => {
    const result = sinhVien.aggregate([
      {
        $group: {
          "_id": "$MaLop",
          TongHB : { "$sum" : "$HocBong"},
        }
      },
      {
        $lookup: {
          from: lop.collection.name,
          localField: "_id",
          foreignField: "_id",
          as: "lop",
        }
      },
      {
        $replaceRoot : { newRoot: { $mergeObjects: [ { $arrayElemAt: ["$lop",0]}, "$$ROOT"]}},
      },
      {
        $project : { lop: 0},
      },
      {
        $project: { TenLop: 1, TongHB: 1},
      }
    ]);
    return result;
  }

  const cau18 = async() => {
    const result = sinhVien.aggregate([
      {
        $lookup: {
          from: lop.collection.name,
          localField: "MaLop",
          foreignField: "_id",
          as: "lop",
        },
      },{
        $replaceRoot: {newRoot: {$mergeObjects: [{$arrayElemAt: ["$lop",0]},"$$ROOT"]}},
      },
      {
        $lookup: {
          from: khoa.collection.name,
          localField: "MaKhoa",
          foreignField: "_id",
          as: "khoa",
        }
      },
      {
        $replaceRoot: {newRoot: {$mergeObjects: [{$arrayElemAt: ["$khoa",0]},"$$ROOT"]}},
      },
      {
        $group: {
          _id: "$MaKhoa",
          TenKhoa: { $first :"$TenKhoa"},
          TongHB: {"$sum": "$HocBong"},
        }
      }

    ])
    return result;
  }

  const cau19 = async() => {
    const result = sinhVien.aggregate([
      {
        $lookup: {
          from: lop.collection.name,
          localField: "MaLop",
          foreignField: "_id",
          as: "lop",
        },
      },
      {
        $replaceRoot: { newRoot: {$mergeObjects: [{$arrayElemAt: ["$lop",0]},"$$ROOT"]}},
      },
      {
        $lookup: {
          from: khoa.collection.name,
          localField: "MaKhoa",
          foreignField: "_id",
          as: "khoa",
        },
      },
      {
        $replaceRoot: { newRoot: {$mergeObjects: [{$arrayElemAt: ["$khoa",0]},"$$ROOT"]}},
      },
      {
        $group: {
          _id: "$MaKhoa",
          TenKhoa: {$first: "$TenKhoa"},
          Slsinhvien : {"$sum": 1},
        },
      },
      {
        $match: {
          Slsinhvien: {$gt: 3},
        }
      }
    ]) ;
    return result;
  }

  const cau20 = async() => {
    const result = sinhVien.aggregate([
      {
        $lookup: {
          from: lop.collection.name,
          localField: "MaLop",
          foreignField: "_id",
          as: "lop",
        },
      },
      {
        $replaceRoot: { newRoot: {$mergeObjects: [{$arrayElemAt: ["$lop",0]},"$$ROOT"]}},
      },
      {
        $match: {
          Nu: "Yes",
        }
      },
      {
        $lookup: {
          from: khoa.collection.name,
          localField: "MaKhoa",
          foreignField: "_id",
          as: "khoa",
        },
      },
      {
        $replaceRoot: { newRoot: {$mergeObjects: [{$arrayElemAt: ["$khoa",0]},"$$ROOT"]}},
      },
      {
        $group: {
          _id: "$MaKhoa",
          TenKhoa: {$first: "$TenKhoa"},
          Slsinhvien : {"$sum": 1},
        },
      },
      {
        $match: {
          Slsinhvien: {$gt: 2},
        }
      }
    ]) ;
    return result;
  }

  const cau21 = async() => {
    const result = sinhVien.aggregate([
      {
        $lookup: {
          from: lop.collection.name,
          localField: "MaLop",
          foreignField: "_id",
          as: "lop",
        },
      },{
        $replaceRoot: {newRoot: {$mergeObjects: [{$arrayElemAt: ["$lop",0]},"$$ROOT"]}},
      },
      {
        $lookup: {
          from: khoa.collection.name,
          localField: "MaKhoa",
          foreignField: "_id",
          as: "khoa",
        }
      },
      {
        $replaceRoot: {newRoot: {$mergeObjects: [{$arrayElemAt: ["$khoa",0]},"$$ROOT"]}},
      },
      {
        $group: {
          _id: "$MaKhoa",
          TenKhoa: { $first :"$TenKhoa"},
          TongHB: {"$sum": "$HocBong"},
        }
      },
      {
        $match: {
          TongHB: {$gt: 93600000},
        }
      }

    ])
    return result;
  }

  const cau22 = async() => {
    const result = sinhVien.aggregate([
      {
        $group: {
          _id: null,
          maxScore : { $max: "$HocBong"},
          svGrap: {
            $push: {
              _id: "$_id",
              HoTen: "$HoTen",
              Nu: "$Nu",
              NgaySinh: "$NgaySinh",
              MaLop: "$MaLop",
              HocBong: "$HocBong",
              Tinh: "$Tinh",
            }
          }
        }
      },
      {
        $project: {
          _id: 0,
          list: {
            $setDifference: [{
              $map: {
                input: "$svGrap",
                as: "sv",
                in:  {
                  $cond: [
                    {
                      "$eq": ["$maxScore","$$sv.HocBong"]
                    },
                    "$$sv",
                    false
                  ]
                }
              }
            }, [false]]
          }
        }
      },
      {
        $unwind: "$list",
      },
      {
        $project: {
          _id: "$list._id",
          HoTen: "$list.HoTen",
          Nu: "$list.Nu",
          NgaySinh: "$list.NgaySinh",
          MaLop: "$list.MaLop",
          HocBong: "$list.HocBong",
          Tinh: "$list.Tinh",
        }
      }
    ])
    return result;
  }

  const cau23 = async () => {
    const result = sinhVien.aggregate([
      {
        $lookup: {
          from: ketQua.collection.name,
          localField: "_id",
          foreignField: "MaSV",
          as: "kq",
        }
      },
      {
        $replaceRoot: {newRoot: { $mergeObjects: [{$arrayElemAt: ["$kq",0]},"$$ROOT"]}},
      },
      {
        $match: {
          MaMH: mongoose.Types.ObjectId('63be8612a0e8ec4a4d3b8470'),
        }
      },
      {
        $group:{
          _id: null,
          maxScore: {$max: "$DiemThi"},
          svGrap: {
            $push: {
              _id: "$_id",
              HoTen: "$HoTen",
              DiemThi: "$DiemThi",
            }
          }
        }
      },
      {
        $project: {
          _id: 0,
          list: {
            $setDifference: [{
              $map: {
                input: "$svGrap",
                as: "sv",
                in: {
                  $cond: [{$eq: ["$maxScore","$$sv.DiemThi"]},"$$sv",false]
                }
              }
            },[false]]
          }
        }
      },
      {
        $unwind: "$list",
      },
      {
        $project: {
          _id: "$list._id",
          HoTen: "$list.HoTen",
          DiemThi: "$list.DiemThi",
        }
      }
    ])
    return result;
  };
  const cau24 = async () => {
    const result = sinhVien.aggregate([
      {
        $lookup: {
          from: ketQua.collection.name,
          localField: "_id",
          foreignField: "MaSV",
          as: "kq",
        },
      },
      {
        $replaceRoot: {newRoot: { $mergeObjects: [{$arrayElemAt: ["$kq",0]},"$$ROOT"]}},
      },
      {
        $match: {
          MaMH: {$nin: [mongoose.Types.ObjectId('63be8612a0e8ec4a4d3b8470')]}
        }
      },
      {
        $group: {
          _id: "$_id",
          MaSV: {$first: "$MaSV"},
          HoTen: {$first: "$HoTen"},
          DiemThi: {$first: "$DiemThi"},
          MaMH: {$first: "$MaMH"},
        }
      }
    ])
    return result;
  };
  const cau25 = async () => {
    const result = sinhVien.aggregate([
      {
        $lookup:
          {
            from: lop.collection.name,
            localField: "MaLop",
            foreignField: "_id",
            as: "lop",
          }
      },
      {
        $replaceRoot: {newRoot: {$mergeObjects: [{$arrayElemAt: ["$lop",0]},"$$ROOT"]}},
      },
      {
        $lookup:
          {
            from: khoa.collection.name,
            localField: "MaKhoa",
            foreignField: "_id",
            as: "khoa",
          }
      },
      {
        $replaceRoot: {newRoot: {$mergeObjects: [{$arrayElemAt: ["$khoa",0]},"$$ROOT"]}},
      },
      {
        $group: {
          _id: "$MaKhoa",
          TenKhoa: {$first: "$TenKhoa"},
          SLsinhvien: {"$sum" : 1},
        }
      },
      {
        $group: {
          _id: 0,
          MaxSl: {"$max" : "$SLsinhvien"},
          list: {
            $push: {
              _id: "$_id",
              TenKhoa: "$TenKhoa",
              SLsinhvien: "$SLsinhvien",
            }
          }
        }
      },
      {
        $project: {
          _id: 0,
          result: {
            $setDifference: [{
              $map: {
                input: "$list",
                as: "kh",
                in: {
                  $cond: [{"$eq": ["$MaxSl","$$kh.SLsinhvien"]},"$$kh",false]
                }
              }
            },[false]]
          }
        }
      },
      {
        $unwind: "$result",
      },
      {
        $project: {
          _id: "$result._id",
          TenKhoa: "$result.TenKhoa",
          SLsinhvien: "$result.SLsinhvien",
        }
      }
    ])
    return result;
  };

module.exports = {
  cau1,
  cau2,
  cau3,
  cau4,
  cau5,
  cau6,
  cau7,
  cau8,
  cau9,
  cau10,
  cau11,
  cau12,
  cau13,
  cau14,
  cau15,
  cau16,
  cau17,
  cau18,
  cau19,
  cau20,
  cau21,
  cau22,
  cau23,
  cau24,
  cau25,
};